// TODO: Search(specific), Update, Delete
// TODO: 3. Build controller for edit html. Should support functionalities listed in edit.html
(function () {
    'use strict';
    angular
        .module("GMS")
        .controller("EditCtrl", EditCtrl);

    EditCtrl.$inject = ["$filter", "grocery_listService", "$stateParams"];

    function EditCtrl($filter, grocery_listService, $stateParams) {

        // Declares the var vm (for ViewModel) and assigns it the object this. Any function or variable that you attach
        // to vm will be exposed to callers of EditCtrl, e.g., edit.html
        var vm = this;

        // Exposed data models -----------------------------------------------------------------------------------------
        // Creates a department object. We expose the department object by attaching it to the vm. This allows us to
        // apply two-way data-binding to this object by using ng-model in our view (i.e., index.html)
        vm.name = "";
        vm.result = {};

        // Exposed functions ------------------------------------------------------------------------------------------
        // Exposed functions can be called from the view.
        vm.deleteManager = deleteManager;
        vm.initDetails = initDetails;
        vm.searchName = searchName;
        vm.toggleEditor = toggleEditor;
        vm.updateName = updateName;
        vm.updateBrand = updateBrand;
        vm.updateupc12 = updateupc12;

        // Initializations --------------------------------------------------------------------------------------------
        // Functions that are run when view/html is loaded
        initDetails();

        //Passing in the params from searchDB
        if($stateParams.name){
            console.log("hello");
            vm.name = $stateParams.name;
            vm.searchName();
        }
        else {console.log("hey")}

        // Initializes department details shown in view
        function initDetails() {
            console.log("-- show.controller.js > initDetails()");
            vm.result.name = "";
            vm.result.brand = "";
            vm.result.upc12 = "";
            vm.showDetails = false;
            vm.isEditorOn = false;
        }

        // Saves edited  name
        function updateName() {
            console.log("-- show.controller.js > save()");
            grocery_listService
                .updategrocery_list(vm.name, vm.result.name)
                .then(function (result) {
                    console.log("-- show.controller.js > save() > results: \n" + JSON.stringify(result.data));
                })
                .catch(function (err) {
                    console.log("--  show.controller.js > save() > error: \n" + JSON.stringify(err));
                });
            vm.toggleEditor();
        }

   // Saves edited brand
        function updateBrand() {
            console.log("-- show.controller.js > save()");
            grocery_listService
                .updateBrand(vm.name, vm.result.brand)
                .then(function (result) {
                    console.log("-- show.controller.js > save() > results: \n" + JSON.stringify(result.data));
                })
                .catch(function (err) {
                    console.log("--  show.controller.js > save() > error: \n" + JSON.stringify(err));
                });
            vm.toggleEditor();
        }

           // Saves edited upc12
        function updateDeptName() {
            console.log("-- show.controller.js > save()");
            DeptService
                .updateDept(vm.name, vm.result.upc12)
                .then(function (result) {
                    console.log("-- show.controller.js > save() > results: \n" + JSON.stringify(result.data));
                })
                .catch(function (err) {
                    console.log("--  show.controller.js > save() > error: \n" + JSON.stringify(err));
                });
            vm.toggleEditor();
        }

        function searchName() {
            console.log("-- show.controller.js > search()");
            initDetails();
            vm.showDetails = true;

            grocery_listService
                .retrievegrocery_listByName(vm.name)
                .then(function (result) {
                    // Show table structure
                    vm.showDetails = true;

                    // This is a good way to understand the type of results you're getting
                    console.log("-- show.controller.js > search() > results: \n" + JSON.stringify(result.data));

                    // Exit .then() if result data is empty
                    if (!result.data)
                        return;

                    // The result is an array of objects that contain only 1 object
                    // We are assigning value like so, so that we don't have to do access complex structures
                    // from the view. Also this would give you a good sense of the structure returned.
                    // You could, of course, massage data from the back end so that you get a simpler structure
                    vm.result.name = result.data.name;
                    vm.result.brand = result.data.brand;
                     vm.result.upc12 = result.data.upc12;
                  
                })
                .catch(function (err) {
                    console.log("--  show.controller.js > search() > error: \n" + JSON.stringify(err));
                });
        }

        // Switches editor state of the department name input/edit field
        function toggleEditor() {
            vm.isEditorOn = !(vm.isEditorOn);
        }
    }
})();