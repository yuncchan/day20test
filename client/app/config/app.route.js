(function(){
    angular
        .module("GMS")
        .config(uiRouteConfig);
    
    uiRouteConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function uiRouteConfig($stateProvider, $urlRouterProvider){
        $stateProvider
            .state("edit", {
                url: "/edit",
                templateUrl: "app/edit/edit.html"
            })
            .state("searchDB", {
                url: "/searchDB",
                templateUrl: "app/search/searchDB.html"
            })
            .state("editWithParams", { 
               url: "/edit/:name",
               templateUrl: "app/edit/edit.html",
           });
    }
})();