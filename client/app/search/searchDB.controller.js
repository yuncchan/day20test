(function () {
    angular
        .module("GMS")
        .controller("SearchDBCtrl", SearchDBCtrl);

    SearchDBCtrl.$inject = ['grocery_listService'];

    function SearchDBCtrl(grocery_listService) {
        var vm = this;

        vm.searchString = '';
        vm.result = null;

        // Exposed functions ------------------------------------------------------------------------------------------
        // Exposed functions can be called from the view.
       vm.searchName = searchName;
        vm.searchBrand = searchBrand;
        

        // Initializations --------------------------------------------------------------------------------------------
        // Functions that are run when view/html is loaded
        // init is a private function (i.e., not exposed)
        init();

        // Function declaration and definition -------------------------------------------------------------------------
        // The init function initializes view
        function init() {
            // We call DeptService.retrieveDeptDB to handle retrieval of department information. The data retrieved
            // from this function is used to populate search.html. Since we are initializing the view, we want to
            // display all available departments, thus we ask service to retrieve '' (i.e., match all)
            grocery_listService
                .retrievegrocery_listDB('')
                .then(function (results) {
                    // The result returned by the DB contains a data object, which in turn contains the records read
                    // from the database
                    vm.grocery_lists = results.data;
                })
                .catch(function (err) {
                    // We console.log the error. For a more graceful way of handling the error, see
                    // register.controller.js
                    console.log("error " + err);
                });
        }


        // The search function searches for departments that matches query string entered by user. The query string is
        // matched against the department name and department number alike.
        function searchName() {
            //vm.showManager = false;
            grocer_listService
                // we pass contents of vm.searchString to service so that we can search the DB for this string
                .retrievegrocery_listDB(vm.searchString)
                .then(function (results) {
                    // The result returned by the DB contains a data object, which in turn contains the records read
                    // from the database
                    vm.grocery_lists = results.data;
                })
                .catch(function (err) {
                    // We console.log the error. For a more graceful way of handling the error, see
                    // register.controller.js
                    console.log("error " + err);
                });
        }


        // The search function searches for departments that matches query string entered by user. The query string is
        // matched against the department name and department number alike.
        function searchBrand() {
            //vm.showManager = true;
            grocery_listService
            // we pass contents of vm.searchString to service so that we can search the DB for this string
                .retrievegrocery_listBrand(vm.searchString)
                .then(function (results){
                    // The result returned by the DB contains a data object, which in turn contains the records read
                    // from the database
                    console.log("results: " + JSON.stringify(results.data));
                    vm.grocery_lists = results.data;
                })
                .catch(function (err) {
                    // We console.log the error. For a more graceful way of handling the error, see
                    // register.controller.js
                    console.info("error " + JSON.stringify(err));
                });
        }
    }
})();