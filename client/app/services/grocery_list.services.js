// Always use an IIFE, i.e., (function() {})();
(function () {
    // Attaches grocery_listService service to the GMS module
    angular
        .module("GMS")
        .service("grocery_listService", grocery_listService);

    // Dependency injection. Here we inject $http because we need this built-in service to communicate with the server
    // There are different ways to inject dependencies; $inject is minification safe
    grocery_listService.$inject = ['$http'];

    // grocery_listService function declaration
    // Accepts the injected dependency as a parameter. We name it $http for consistency, but you may assign any name
    function grocery_listService($http) {

        // Declares the var service and assigns it the object this (in this case, the grocery_listService). Any function or
        // variable that you attach to service will be exposed to callers of grocery_listService
        var service = this;

        // EXPOSED DATA MODELS -----------------------------------------------------------------------------------------
        // EXPOSED FUNCTIONS -------------------------------------------------------------------------------------------
        service.deletegrocery_list = deletegrocery_list;
        service.retrievegrocery_list = retrievegrocery_list;
        service.retrievegrocery_listDB = retrievegrocery_listDB;
        service.retrievegrocery_listByName = retrievegrocery_listByName;
        service.updategrocery_list = updategrocery_list;


        // FUNCTION DECLARATION AND DEFINITION -------------------------------------------------------------------------

        // deletegrocery_list uses HTTP DELETE to delete grocery_list row from database; passes information as route parameters.
        // IMPORTANT! Route parameters are not the same as query strings!
        function deletegrocery_list(name) {
            return $http({
                method: 'DELETE'
                , url: 'api/departments/' + name
            });

        }

        // retrieveDept retrieves department information from the server via HTTP GET.
        // Parameters: None. Returns: Promise object
        function retrievegrocery_list() {
            return $http({
                method: 'GET'
                , url: 'api/static/grocery_lists'
            });
        }

        
        // string (params) Parameters: searchString. Returns: Promise object
        function retrievegrocery_listDB(searchString) {
            return $http({
                method: 'GET'
                , url: 'api/grocery_lists'
                , params: {
                    'searchString': searchString
                }
            });
        }

        // retrievegrocery_listByName retrieves grocery_list information from the server via HTTP GET. Passes information as a
        // route parameter
        function retrievegrocery_listByName(name) {
            return $http({
                method: 'GET'
                , url: "api/grocery_lists/" + name
            });
        }

        

        // updategrocery_list uses HTTP PUT to update department name saved in DB; passes information as route parameters and via
        // HTTP HEADER BODY IMPORTANT! Route parameters are not the same as query strings!
        function updategrocery_list(name) {
            return $http({
                method: 'PUT'
                , url: 'api/grocery_lists/' + name
                , data: {
                    name: name
                }
            });
        }
    }
})();