// TODO: 3.1 Deifine departments table model
//Model for departments table
// References:
// http://docs.sequelizejs.com/en/latest/docs/getting-started/#your-first-model
// http://docs.sequelizejs.com/en/latest/docs/models-definition/
module.exports = function (sequelize, Sequelize) {
    var grocery_list = sequelize.define("grocery_lists",
        {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                allowNull: false
            },
            
            name: {
                type: Sequelize.STRING,
                allowNull: false
            },
            brand: {
                type: Sequelize.STRING,
                allowNull: false
            },
            
            upc12: {
                type: Sequelize.BIGINT,
                allowNull: false
            },
            

        }, {
            tableName:"grocery_list",
            timestamps: false
        }
        );

    return grocery_list;
};