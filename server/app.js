
// DEPENDENCIES ------------------------------------------------------------------------------------------------------
// Loads express module and assigns it to a var called express
var express = require("express");

// Loads path to access helper functions for working with files and directory paths
var path = require("path");

// Loads bodyParser to populate and parse the body property of the request object
var bodyParser = require("body-parser");

// Loads sequelize ORM
var Sequelize = require("sequelize");

// CONSTANTS ---------------------------------------------------------------------------------------------------------
// Defines server port.
// Value of NODE_PORT is taken from the user environment if defined; port 3000 is used otherwise.
const NODE_PORT = process.env.NODE_PORT || 3000;

// Defines paths
// __dirname is a global that holds the directory name of the current module
const CLIENT_FOLDER = path.join(__dirname, "/../client");  // CLIENT FOLDER is the public directory
//const MSG_FOLDER = path.join(CLIENT_FOLDER, '/assets/messages');

// Defines MySQL configuration
const MYSQL_USERNAME = "root";
const MYSQL_PASSWORD = "wAve1";

// OTHER VARS ---------------------------------------------------------------------------------------------------------
// Creates an instance of express called app
var app = express();

// DBs, MODELS, and ASSOCIATIONS ---------------------------------------------------------------------------------------
// Creates a MySQL connection
var sequelize = new Sequelize(
    "shop",
    MYSQL_USERNAME,
    MYSQL_PASSWORD,
    {
        // default port    : 3306
        host: "localhost",         
        logging: console.log,
        dialect: "mysql",
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    }
);

// Loads model for grocery_list table
var grocery_list = require("./models/grocery_list")(sequelize, Sequelize);

// MIDDLEWARES --------------------------------------------------------------------------------------------------------

// Serves files from public directory (in this case CLIENT_FOLDER).
// __dirname is the absolute path of the application directory.
// if you have not defined a handler for "/" before this line, server will look for index.html in CLIENT_FOLDER
app.use(express.static(CLIENT_FOLDER));

// Populates req.body with information submitted through the registration form.
// Default $http content type is application/json so we use json as the parser type
// For content type is application/x-www-form-urlencoded  use: app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());


// ROUTE HANDLERS -----------------------------------------------------------------------------------------------------


// Defines endpoint handler exposed to client side for retrieving department information from database
app.get("/api/grocery_list", function (req, res) {
    if(!req.query.searchString) req.query.searchString="";
    grocery_list
    // findAll asks sequelize to search
        .findAll({
            limit:20,
            order: [["name", "ASC"]],
            where: {
                $or: [
                    {name: {$like: "%" + req.query.searchString + "%"}},
                    {brand: {$like: "%" + req.query.searchString + "%"}},
                ]
            }
        })
        .then(function (grocery_list) {
            res
                .status(200)
                .json(grocery_list);
        })
        .catch(function (err) {
            res
                .status(500)
                .json(err);
        });
});


app.get("/api/grocery_lists/:name", function (req, res) {
    console.log
    var where = {};
    if (req.params.name) {
        where.name = req.params.name
    }

    console.log("where " + where);

   grocery_list
        .findOne({
            where: where

        })
        .then(function (grocery_lists) {
            console.log("-- GET /api/grocery_lists/:name findOne then() result \n " + JSON.stringify(grocery_lists));
            res.json(grocery_lists);
        })
        // this .catch() handles erroneous findAll operation
        .catch(function (err) {
            console.log("-- GET /api/departments/:dept_no findOne catch() \n " + JSON.stringify(grocery_lists));
            res
                .status(500)
                .json({error: true});
        });

});


// -- Updates department info
app.put('/api/grocery_lists/:name', function (req, res) {
    var where = {};
    where.name = req.params.name;

    // *** Updates department detail
    
});


// ERROR HANDLING ----------------------------------------------------------------------------------------------------
// Handles 404. In Express, 404 responses are not the result of an error,
// so the error-handler middleware will not capture them.
// To handle a 404 response, add a middleware function at the very bottom of the stack
// (below all other path handlers)
app.use(function (req, res) {
    res.status(404).sendFile(path.join(MSG_FOLDER + "/404.html"));
});

// Error handler: server error
app.use(function (err, req, res, next) {
    res.status(501).sendFile(path.join(MSG_FOLDER + '/501.html'));
});


// SERVER / PORT SETUP ------------------------------------------------------------------------------------------------
// Server starts and listens on NODE_PORT
app.listen(NODE_PORT, function () {
    console.log("Server running at " + NODE_PORT);
});